// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;


library Counters {
    struct Counter {
        // This variable should never be directly accessed by users of the library: interactions must be restricted to
        // the library's function. As of Solidity v0.5.2, this cannot be enforced, though there is a proposal to add
        // this feature: see https://github.com/ethereum/solidity/issues/4637
        uint256 _value; // default: 0
    }

    function current(Counter storage counter) internal view returns (uint256) {
        return counter._value;
    }

    function increment(Counter storage counter) internal {
        unchecked {
            counter._value += 1;
        }
    }

    function decrement(Counter storage counter) internal {
        uint256 value = counter._value;
        require(value > 0, "Counter: decrement overflow");
        unchecked {
            counter._value = value - 1;
        }
    }

    function reset(Counter storage counter) internal {
        counter._value = 0;
    }
}

abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot's contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler's defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.

    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction's gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;

    uint256 private _status;

    constructor() {
        _status = _NOT_ENTERED;
    }

    /**
     * @dev Prevents a contract from calling itself, directly or indirectly.
     * Calling a `nonReentrant` function from another `nonReentrant`
     * function is not supported. It is possible to prevent this from happening
     * by making the `nonReentrant` function external, and make it call a
     * `private` function that does the actual work.
     */
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, "ReentrancyGuard: reentrant call");

        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;

        _;

        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}


contract VLM is ReentrancyGuard  {
    
  using Counters for Counters.Counter;
  Counters.Counter private _recordIds;

  address payable owner;


  constructor() {
    owner = payable(msg.sender);
  }

   modifier onlyOwner {
      require(msg.sender == owner);
      _;
   }

  struct Record {
    uint256 recordId;
    uint256 engineNumber;
    uint256 chassisNumber;
    uint256 invoicedAmount;
    uint256 date;
    string name;
    string gender;
    uint256 mobileNumber;
    uint256 dateOfBirth;
    uint256 aadharNumber;
    string addressProof;
    string insurerCompany;
    uint256 insuredAmount;
  }

  mapping(uint256 => Record) private idToRecord;

  event RecordCreated (
   uint256 recordId,
    uint256 engineNumber,
    uint256 chassisNumber,
    uint256 invoicedAmount,
    uint256 date,
    string name,
    string gender,
    uint256 mobileNumber,
    uint256 dateOfBirth,
    uint256 aadharNumber,
    string addressProof,
    string insurerCompany,
    uint256 insuredAmount
    ); 

  event updateRecord(address indexed from, uint256 chassisNumber);
  event deleteRecord(address indexed from, uint256 chassisNumber);

  /* Creating Record */
  function createRecord(
    uint256 engineNumber,
    uint256 chassisNumber,
    uint256 invoicedAmount,
    uint256 date,
    string memory name,
    string memory gender,
    uint256 mobileNumber,
    uint256 dateOfBirth,
    uint256 aadharNumber,
    string  memory addressProof,
    string memory insurerCompany,
    uint256 insuredAmount
  ) public onlyOwner nonReentrant {

    _recordIds.increment();
    uint256 recordId = _recordIds.current();
  
    idToRecord[recordId] =  Record(
    recordId,
    engineNumber,
    chassisNumber,
    invoicedAmount,
    date,
    name,
    gender,
    mobileNumber,
    dateOfBirth,
    aadharNumber,
    addressProof,
    insurerCompany,
    insuredAmount
    );

    emit RecordCreated(
    recordId,
    engineNumber,
    chassisNumber,
    invoicedAmount,
    date,
    name,
    gender,
    mobileNumber,
    dateOfBirth,
    aadharNumber,
    addressProof,
    insurerCompany,
    insuredAmount
    );
  }

  function updateRecordOwnership(uint256 chassisNumber, string memory newOwnerName, string memory gender, uint256 mobileNumber, uint256 aadharNumber) public onlyOwner returns(bool) {
      Record storage currentRecord = idToRecord[chassisNumber];
      currentRecord.name = newOwnerName;
      currentRecord.gender = gender;
      currentRecord.mobileNumber = mobileNumber;
      currentRecord.aadharNumber = aadharNumber;
      emit updateRecord(msg.sender, chassisNumber);
      return true;
  }

  function deleteRecordByChassisNumber(uint256 chassisNumber) public onlyOwner returns (bool) {
    delete  idToRecord[chassisNumber];
    emit deleteRecord(msg.sender, chassisNumber);
    return true;
  }

  
  /* Returns all Records */
  function queryAllRecords() public view returns (Record[] memory) {
    uint recordCount = _recordIds.current();
    uint currentIndex = 0;
    Record[] memory records = new Record[](recordCount);
    for (uint i = 0; i < recordCount; i++) {
        uint currentId = i + 1;
        Record storage currentRecord = idToRecord[currentId];
        records[currentIndex] = currentRecord;
        currentIndex += 1;
    }
    return records;
  }

  /* Returns records by chassisNumber */
  function queryRecordByChassisNumber(uint256 chassisNumber) public view returns (Record memory) {
    Record storage record = idToRecord[chassisNumber];
    return record;
  }
  
  function totalRecords() public view returns (uint256) {
  return _recordIds.current();
  }

}