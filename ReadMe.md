Contract Deployment Steps : 

please make sure you have installed node, npm and truffle in your machine.

Step-1:  Create .secret file and paste your mnemonic key

Step-2:  In truffle-config.js we need to replace Infura Project ID

Step-3:  Command for compile and deploy Using truffle

         truffle compile

         For deploy
         truffle deploy --network  rinkeby
         or
         truffle deploy --network rinkeby --reset 
