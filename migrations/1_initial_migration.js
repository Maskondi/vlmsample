const Migrations = artifacts.require("Migrations");
const VLM = artifacts.require("VLM");

module.exports = function (deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(VLM);
  console.log("contract deployed successfully");
};
